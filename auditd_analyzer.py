"""
Auditd log analyzer written by Nir Bromberg
parsed_db_example.db <- File run through this program
settings.conf <- Configuration file for different program parameters
Tested and working in 5.15.0-56-generic #62~20.04.1-Ubuntu SMP
"""


import logging
import glob
import sqlite3
import re

logging.basicConfig(level=logging.DEBUG, format="%(levelname)s: %(message)s")
analyzer_configuration = {}


def read_configuration_file(file_name):
    """
    :param file_name: The function fills the configuration dict based on the config file supplied
    :return: None
    """
    try:
        with open(file_name) as f:
            settings = f.read().splitlines()

            for setting in settings:
                name, value = setting.split('=')
                analyzer_configuration[name] = value

            logging.info("Read configuration file successfully")
    except ValueError:
        logging.critical("Configuration file is corrupt or invalid")
        exit(1)
    except FileNotFoundError:
        logging.critical("Could not find file {}".format(file_name))
        exit(1)
    except Exception as err:
        logging.critical("Unexpected error opening file: {}, is {}".format(file_name, err))
        exit(1)


def find_logs():
    """
    This function finds the log file paths in the supplied working directory
    :return: The log file paths in string format
    """
    if analyzer_configuration.get("WORK_DIRECTORY"):
        log_files = list

        if analyzer_configuration["WORK_DIRECTORY"].endswith("/"):
            log_files = glob.glob(analyzer_configuration["WORK_DIRECTORY"] + "*")
        else:
            log_files = glob.glob(analyzer_configuration["WORK_DIRECTORY"] + "/*")

        if not log_files:
            logging.warning("No log files found in directory: {}".format(analyzer_configuration["WORK_DIRECTORY"]))
        else:
            logging.info("Found log files in work directory")

        return log_files
    else:
        logging.critical("Could not find param WORK_DIRECTORY in config file")
        exit(1)


def table_exists(db_con, table_name):
    """
    This function checks if a table of a certain name exists in the database
    :param db_con: the handle to the database
    :param table_name: the table name
    :return: true or false if the table exists in the database
    """
    cursor = db_con.cursor()
    rows = cursor.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name= '{}'""".format(table_name)).fetchall()

    if rows:
        cursor.close()
        return True

    cursor.close()
    return False


def get_column_names(db_con, table_name):
    """
    This function gets the names of all columns in the table
    :param db_con: the handle to the database
    :param table_name: the name of the table
    :return: a list of column names
    """
    cursor = db_con.cursor()

    cursor.execute("SELECT * FROM {}".format(table_name))
    names = [description[0] for description in cursor.description]

    cursor.close()

    return names


def does_id_exist(db_con, entry_id, table_name):
    """
    This function checks if an id exists within the database
    :param db_con: The handle to the database
    :param entry_id: the id we are searching for
    :param table_name: the name of the table to search the id in
    :return: true or false if id exists
    """
    cursor = db_con.cursor()

    cursor.execute("""SELECT * FROM {} WHERE SERIAL_ID={}""".format(table_name, entry_id))

    exists = len(cursor.fetchall()) >= 1

    cursor.close()

    return exists


def construct_insert_values(entry_dict):
    """
    This function constructs a values string for a database insertion query
    :param entry_dict: The dict of keys and values
    :return: returns the string created
    """
    dict_values = list(entry_dict.values())

    insertion_int_values = ', '.join(list(entry_dict.values())[:2]) + ', '
    insertion_string_values = ', '.join(['"' + string + '"' for string in dict_values[2:]])
    insertion_string = insertion_int_values + insertion_string_values
    return insertion_string


def insert_log_entry(db_con, cursor, entry_dict, table_name):
    """
    This function inserts a log entry via the entry dict
    :param db_con: The database handle
    :param cursor: The cursor of the database
    :param entry_dict: Entry dict containing key-value pairs
    :param table_name: The table to insert into
    :return: None
    """
    insertion_string = construct_insert_values(entry_dict)
    col_names = ', '.join(entry_dict.keys())
    cursor.execute("""INSERT INTO {}({}) VALUES({})""".format(table_name, col_names, insertion_string))
    db_con.commit()


def parse_logs(log_paths, db_con):
    """
    This function parses the logs using regex, separates different entry types into tables and sets parameters to be
    the column tables, using regex
    :param log_paths: a list of the log file paths
    :param db_con: a handle to the database
    :return: None
    """
    cursor = db_con.cursor()

    try:
        for log_path in log_paths: # Multiple logs might be present in work directory
            with open(log_path) as log:
                type_pattern = re.compile("(type=.*)")
                entries = re.findall(type_pattern, log.read())

            for entry in entries:
                entry_dict = {}

                header_pattern = re.compile("type=(.*) msg=audit\((.*)\): ")
                type_name, audit = re.findall(header_pattern, entry)[0]
                timestamp, serial_id = audit.split(":")

                entry_dict["SERIAL_ID"] = serial_id
                entry_dict["TIMESTAMP"] = timestamp

                data_pattern = re.compile("type=.* msg=audit\(.*\): (.*)")
                data = re.findall(data_pattern, entry)[0]

                parameter_pattern = re.compile("(\w+)=('[^']+'|\"[^\"]+\"|[^\s]+)(?=\s|$)") # If variable is in quotations ignore spaces
                parameters = re.findall(parameter_pattern, data)

                for parameter_name, parameter_value in parameters:
                    entry_dict[parameter_name.upper()] = parameter_value.replace('"', "'")

                if "UNKNOWN" in type_name: # Unknown processes could contain different PIDs so we include it like so
                    type_name = "ID_" + f"{type_name[8:-1]}_" + "UNKNOWN"

                table_name = type_name + "S" # Adding an S for plural table names

                col_keys = list(entry_dict.keys())

                if not table_exists(db_con, table_name):
                    col_names_creation = 'SERIAL_ID INTEGER NOT NULL, TIMESTAMP REAL NOT NULL' + ', ' + ' TEXT, '.join(col_keys[2:]) + " TEXT"

                    cursor.execute("""CREATE TABLE IF NOT EXISTS {} ({})""".format(table_name, col_names_creation))

                    logging.info("Created table {}".format(table_name))
                    db_con.commit()

                    insert_log_entry(db_con, cursor, entry_dict, table_name)
                else:
                    names = get_column_names(db_con, table_name)

                    if does_id_exist(db_con, serial_id, table_name):
                        continue

                    for column_name in col_keys:
                        if column_name not in names:
                            cursor.execute("""ALTER TABLE {} ADD {} TEXT""".format(table_name, column_name))
                            db_con.commit()

                            copy_value = entry_dict[column_name]

                            entry_dict.pop(column_name) # Since the column didn't exist before it needs to be last in the dict
                            entry_dict[column_name] = copy_value

                    insert_log_entry(db_con, cursor, entry_dict, table_name)

        cursor.close()
    except sqlite3.Error as er:
        logging.critical("Sqlite query failed, database might be corrupt: {}".format(err))

    if log_paths:
        logging.info("Finished parsing log files")


def init_database():
    """
    This function connects to a database supplied in the config file
    :return: The handle to the database
    """
    if analyzer_configuration.get("DATABASE_PATH"):
        conn = sqlite3.connect(analyzer_configuration["DATABASE_PATH"])
        logging.info("Database connection established")

        return conn
    else:
        logging.critical("No database path supplied in configuration file")
        exit(1)


def main():
    read_configuration_file("settings.conf")
    log_paths = find_logs()
    connection = init_database()
    parse_logs(log_paths, connection)
    connection.close()


if __name__ == "__main__":
    main()
